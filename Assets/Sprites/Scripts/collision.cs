﻿using UnityEngine;
using System.Collections;

public class collision : MonoBehaviour {
    public GameObject guard;
    public GameObject prisoner;
    public float speed = 300.0f;

    public Transform rayStart, rayEnd;
    public static bool spotted = false;
    public bool PermSpot=false;
    public GameObject arrow;
    //public AudioSource siren;
    public bool facingfront = true;
    //Collision2D coll;
    // Use this for initialization

    

    void Start () {
	InvokeRepeating("patrol",0f,Random.Range(2f,4f));
    //siren = GetComponent<AudioSource>();
        
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<Animator>().enabled = false;           
     
        rayCasting();
        //transform.position = Vector3.MoveTowards(guard.transform.position, prisoner.transform.position + direction, speed * Time.deltaTime);
	}
    void rayCasting()
    {
        
        Vector3 direction = prisoner.transform.position - guard.transform.position;
        //float range = rayEnd.position.y - rayStart.position.y;
        Debug.DrawLine(rayStart.position, rayEnd.position, Color.blue);
        //spotted = Physics2D.CircleCast(rayStart.position, range, rayEnd.position, 1 << LayerMask.NameToLayer("player"));
        spotted = Physics2D.Linecast(rayStart.position, rayEnd.position,1<<LayerMask.NameToLayer("player"));
        if (spotted == true)
        {
            PermSpot = true;}
        if(PermSpot){
            arrow.SetActive(true);
        //siren.SetActive(true);
       //     siren.Play();  

            GetComponent<Animator>().enabled = true;           
     
            transform.position = Vector2.MoveTowards(guard.transform.position, prisoner.transform.position + direction, speed * Time.deltaTime);
            //guard.transform.Rotate(0, 0.0f, prisoner.transform.rotation.z);
            Quaternion rotation = Quaternion.LookRotation
                       (prisoner.transform.position - transform.position, transform.TransformDirection(Vector3.back));
            transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
        }
        else
        {
            arrow.SetActive(false);
            //siren.SetActive(false);
      //      siren.Stop();
        }
    //transform.position = Vector3.MoveTowards(guard.transform.position, prisoner.transform.position + direction, speed * Time.deltaTime);
	
        }
    void patrol()
    {
        facingfront = !facingfront;
        if (facingfront == true)
        {
            //guard.transform.eulerAngles = new Vector2(0, 0);
            guard.transform.Rotate(0, 0, 0);
         
        }
        else
        {
            //guard.transform.eulerAngles = new Vector2(0, 90);
            guard.transform.Rotate(0, 0, 90);
         
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {

        if (coll.gameObject.name == "prisoner 2")
        {
            //GUI.BeginGroup(r(Screen.width / 2, Screen.height / 2, 100, 100));
            //GUI.Label(r(0, 0, 100, 100), "Hit");
            //GUI.EndGroup();
            ////Destroy(coll.gameObject);

     //       GUI.TextArea(new Rect((Screen.width / 2), (Screen.height / 2), 10, 10), "You r caught!");
            Application.LoadLevel("GameOver");
        }
    }

}
