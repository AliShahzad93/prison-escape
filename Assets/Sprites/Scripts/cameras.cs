﻿using UnityEngine;
using System.Collections;

public class cameras : MonoBehaviour {
    public Transform rayStart, rayEnd;
    public bool spotted = false;
    public GameObject arrow;
    public GameObject siren;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        rayCasting();
        
	}
    void rayCasting()
    {
        // Vector3 direction = prisoner.transform.position - guard.transform.position;
        //float range = rayEnd.position.y - rayStart.position.y;
        Debug.DrawLine(rayStart.position, rayEnd.position, Color.blue);
        //spotted = Physics2D.CircleCast(rayStart.position, range, rayEnd.position, 1 << LayerMask.NameToLayer("player"));
        spotted = Physics2D.Linecast(rayStart.position, rayEnd.position, 1 << LayerMask.NameToLayer("player"));
        if (spotted == true)
        {
            arrow.SetActive(true);
            siren.SetActive(true);

        }
        else
        {
            arrow.SetActive(false);
            siren.SetActive(false);

        }
        //transform.position = Vector3.MoveTowards(guard.transform.position, prisoner.transform.position + direction, speed * Time.deltaTime);

    }
    
}
